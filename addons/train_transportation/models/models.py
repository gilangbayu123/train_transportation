# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from odoo import models, fields, api


class train_transportation(models.Model):
    _name= 'train_transportation.train_transportation'
    _inherit = 'train_transportation.order'
    
    train = fields.Boolean('Train')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('open', 'In Progress'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', track_sequence=3, default='draft')

class TrainSession(models.Model):
    _name = 'traini.session'
    _description = 'Train Sesi'

    course_id = fields.Many2one('train.course', string='Nama Tiket', required=True, ondelete='cascade')
    name = fields.Char(string='Nama', required=True)
    start_date = fields.Date(string='Code')
    duration = fields.Float(string='City')
    partner_id = fields.Many2one('res.partner', string='Instruktur')

    _sql_constraints = [
        ('nama_kursus_unik', 'UNIQUE(name)', 'Nama Tiket harus unik'), 
        ('nama_keterangan_cek', 'CHECK(name != description)', 'Nama Tiket dan keterangan tidak boleh sama ')
    ]
    
     
    @api.multi
    def action_confirm(self):
        super(TrainOrder, self).action_confirm()
        if self.train:
            self.write({'state': 'open'})
        return True
    
    def print_train_order(self):
        return self.env.ref('aa_train.action_train_order').report_action(self)


    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            if vals.get('train'):
                vals['name'] = self.env['ir.sequence'].next_by_code('train.order')
            else:
                if 'company_id' in vals:
                    vals['name'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code('sale.order') or _('New')
                else:
                    vals['name'] = self.env['ir.sequence'].next_by_code('sale.order') or _('New')

        # Makes sure partner_invoice_id', 'partner_shipping_id' and 'pricelist_id' are defined
        if any(f not in vals for f in ['partner_invoice_id', 'partner_shipping_id', 'pricelist_id']):
            partner = self.env['res.partner'].browse(vals.get('partner_id'))
            addr = partner.address_get(['delivery', 'invoice'])
            vals['partner_invoice_id'] = vals.setdefault('partner_invoice_id', addr['invoice'])
            vals['partner_shipping_id'] = vals.setdefault('partner_shipping_id', addr['delivery'])
            vals['pricelist_id'] = vals.setdefault('pricelist_id', partner.property_product_pricelist and partner.property_product_pricelist.id)
        result = super(TrainOrder, self).create(vals)
        return result
        

        
class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def print_receipt(self):
    	return self.env.ref('aa_laundry.action_report_receipt').report_action(self)
